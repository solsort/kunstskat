# Kulturdata .DK

Visionen er at gøre det lettere at finde kunstværker og kulturartifakter på nettet, på tværs af kulturinstitutionerne:

- En unik webadresse for hvert kunstværk eller genstand, hvor værket vises og kan lede den besøgende hen til kulturinstitutionen hvor værket findes, såsom [SMK Open](https://open.smk.dk/), [Nationalmuseets Samlinger](https://samlinger.natmus.dk/), [Nivaagaard Malerisamling](https://www.nivaagaard.dk), eller det lokale bibliotek.
- Inspiration – eksempelvis værker af samme kunster på tværs af institutioner, billeder der ligner visuelt, genstande fra samme periode og sted, eller bøger på biblioteket om kunstneren eller emnet.
- Præsentationer af informationer om værket i ensartet form, der både kan forstås af mennesker og [maskiner](https://en.wikipedia.org/wiki/Semantic_Web), – dette vil også gøre at det bliver lettere at finde værket via søgemaskiner såsom google.
- Berigelse af informationer om værkerne via machine learning, som gøre det lettere at finde det, samt give relevant inspiration.

Projektet statede på [Hack4DK](https://hack4.dk) 2019, – og var vinderprojektet ved denne hackathonen.

**Vær med!** – send en mail til kulturdata@solsort.dk, hvis du har lyst til at være med, eller blot vil have besked når der er nyt fra projektet :)

Forøjeblikket videreudvikles projektet for sjov af [Rasmus Erik](https://rasmuserik.com), og det vil være fedt at have flere samarbejdspartnere, både med domæneviden (såsom folk fra kulturinstitutionerne), flair for design, og flere datanørder.

**Ses foreløbig udgave på [kulturdata.dk/work/](https://kulturdata.dk/work/)**. Status:

- Den er ikke helt klart endnu, – eksempelvis er inspirationen tilfældig, der vises kun få metadata, og designet er mangelfuldt. [Kan allerede forstås af maskiner](https://search.google.com/structured-data/testing-tool#url=https%3A%2F%2Fkulturdata.dk%2Fwork%2Fsmk%253AKKS14227).
- Eksempel på berigelse af informationer (og lidt kode for dette) kan ses på [gitlab.com/solsort/2019-natmus-images](https://gitlab.com/solsort/2019-natmus-images/blob/master/sort-images.ipynb). Er i dialog med nationalmuseet om at gå videre med dette.
- Den kommer til at ligge på både: *kunstskat.dk* med kunstværker, *bibdata.dk* med biblioteksmaterialer (og erstatter dette site), samt *kulturdata.dk* for museumsgenstande og øvrige værker. Anbefalingerne kommer til at gå på tværs af disse.
- Kildekoden kan findes på [gitlab.com/solsort/kunstskat](https://gitlab.com/solsort/kunstskat), hvor den videre udvikling også vil blive koordineret.


## Description and details in english.

This is a project, started at [Hack4DK 2019](https://hack4.dk), with the purpose of making a tool where you can explore art and cultural artifacts from different danish institutions. It also has the purpose of making the works findable in search engines, such as google. It is still in progress (this page will be replaced with the actual tool when it reaches MVP-quality).

**You can see a quick preview of its current state on: [kulturdata.dk/work/](/work/)**

The idea is to use the major part of the view, to actually show the work, – below that is the metadata, which you can scroll to, and in the bottom there is a list of related works.

DONE:
-   Experiment with machine learning for enriching the metadata [gitlab.com/solsort/2019-natmus-images](https://gitlab.com/solsort/2019-natmus-images/blob/master/sort-images.ipynb), currently just a proof-of-concept, but will most likely be developed further in collaboration with the museum.
-   Extract data from SMK api
-   Scrape Nivaagaard data
-   Extract library data from bibdata.dk
-   Project setup on [gitlab.com/solsort/kunstskat](https://gitlab.com/solsort/kunstskat)
-   Normalise data models to schema.org
-   Webserver running on kulturdata.dk and kunstskat.dk to be used for this project
-   Simple rendering of work, (only minimal meta data so far)
-   Set up server-side rendering of works for better indexability
-   Setup simple view of recommendations (kind of random at the moment)
-   Semantic markup, i.e. [understandable by search engines](https://search.google.com/structured-data/testing-tool#url=https%3A%2F%2Fkulturdata.dk%2Fwork%2Fsmk%253AKKS14227)

TODO:
-   **Link to the artifact on the institutions site, to lead the visitors to the museums, (and maybe also open hours, address, etc.)**
-   Proper design.
-   Extra data source: National museum (started on this, but on pause due to a 10.000 item limit)
-   Extra data source: Europeana (requested and got API key, haven't looked more into this yet)
-   Better recommendations of works, instead of randomness
-   Show more metadata
-   Include markup like OGP, html-metatags etc.
-   *Use the kultudata.dk implementation as next version of [bibdata.dk](https://bibdata.dk) when it has feature parity*
-   Make a version on kunstskat.dk, containing only art like paintings and sculptures
-   Better showing the work
    -   Choose between different views of same work
    -   Zoomable images for large images
    -   360° viewer for natmus images (incl. VR)
    -   3D object viewer for sculptures (incl. VR)
-   Page per creator
-   Make it searchable
-   Move this todo-list to the gitlab project, after the presentation on SMK ;)

Contact [Rasmus Erik](https://rasmuserik.com) if you have comments, or feel like joining this hack, to make it more than a hack :)



# Forskellige noter

Hack4DK 2019 projekt.

Formålet er at formidle kunst på tværs af institutioner, med et website hvor man kan gå på opdagelse mellem værker.

## Development

- `npm run dev` autocompiles client.js to public_html/main.min.js
- `./dev.sh` run server and restarts it on change
- `npx live-server public_html` runs auto reloading server.



## Tasks

- √udtræk af datadumps
    - followup europeana
    - followup natmus
- √konvertering af data til schema, og gem i elastic
- værkvisning
- relaterede værker
- søgning

## Data

id–prefix:

- smk/XXX
- bib/XXX
- natmus/XXX
- nivaagaard/XXX

schema.org-thing

- type
- name
- description
- identifier
- image
- sameAs
- url kulturdata.dk/smk/...

schema.org-creative-work

- additionalType
- creator
- contributor
- temporal
- keywords
- license
- material
- thumbnailUrl

----

- collection:  smk/bib/natmus/nivaagaard

## RåData
Rådata ligger i `/data/*.jsonl.gz`
### Statens museum for kunst

extract from API instead

### NatMus

https://docs.google.com/document/d/1LsFv5_iWbjoQU8oz-Y7dit5dyyeHKZR0ixvRkcrolmM/edit

NB: no more than 10.000 extracted...
```
http://api.natmus.dk/search/public/simple?query=type%3Aobject+AND+_exists_%3Arelated.assets&size=100&offset=100
```

http://samlinger.natmus.dk/KMM/61213/thumbnail 

### Europeana???

### Nivaagård

Data scraped from nivaagaard.dk, instead of submitted data.

## Backlog idéer

Berigelse af data via neuralt netværk (senere)

- NB: nationalmuseets objekter – info om det er 
- billeder + app på IPFS

## Approach 

÷ nextjs!

- view.js - react view + export
- server.js server side rendering + routing, express app, and leveldb data server
- related dynamic load

- /work/smk:... /work/bib:.. /...

- build
    - webpack auto rebuild on client.js change
- client.js – react-js
    - bind to dom if
    - rendering of work-object
    - rendering of recommendations
    - static html + downloadable json
- server.js – pure js
    - express host
    - host static files
    - react page rendering
    - (work api, related api) – start out with bib
