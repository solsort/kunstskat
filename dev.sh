(sleep 1; touch server.js) &
while inotifywait -e modify,close_write,move_self -q * data/*.js public_html/*
do 
  kill -9 `cat .pid`
  sleep 0.3
  clear;
  #node data/loaddata.js &
  node server.js &
  echo $! > .pid
  sleep 1
done
