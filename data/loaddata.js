const fs = require("fs");
const zlib = require("zlib");
const readline = require("readline");
const _ = require("lodash");

let ids = [];
let count = 0;
let bulk = "";
async function processLines(filename, fn) {
  const stream = fs.createReadStream(filename);
  const gzip = zlib.createGunzip();
  let rl = readline.createInterface({
    input: stream.pipe(gzip),
    crlfDelay: Infinity
  });
  for await (const line of rl) {
    if (++count % 10000 === 0) {
      console.log(count);
    }
    if (line) {
      let o = await fn(JSON.parse(line));
      if (o) {
        ids.push(o.id);
        await db.put(o.id, JSON.stringify(o));
      }
    }
  }
}

async function smk(o) {
  const { object_number } = o;
  const creator = [];
  const contributor = [];
  let materials = [];
  const image = [o.image_native];
  const thumbnail = [o.image_thumbnail.replace("!1600", "!400")];
  let related = [];

  o.alternative_images &&
    o.alternative_images.map(o => {
      if (o.native) {
        image.push(o.native);
        thumbnail.push(o.thumbnail.replace("!1600", "!400"));
      }
    });
  o.production &&
    o.production.map(o => {
      if (o.creator) {
        if (!o.creator_role) {
          creator.push(o.creator);
        } else {
          contributor.push(o.creator);
        }
      }
    });
  o.techniques && o.techniques.map(o => materials.push(o.technique));
  o.materials && o.materials.map(o => materials.push(o.material));
  o.object_names && o.object_names.map(o => materials.push(o.name));

  o.parts && (related = related.concat(o.parts));
  o.part_of && (related = related.concat(o.part_of));

  materials = _.uniq(materials);

  const result = {
    id: "smk:" + object_number,
    name: o.titles && o.titles[0] && o.titles[0].title,
    creator,
    contributor,
    description: "",
    collection: "smk",
    materials,
    image,
    thumbnailUrl: o.image_thumbnail.replace("!1600", "!400"),
    license: o.rights,
    temporal: o.production_date && (o.production_date[0].period || "").trim(),
    sameAs: [],
    keywords: [],

    startYear:
      o.production_date &&
      o.production_date[0].start &&
      o.production_date[0].start.slice(0, 4),
    endYear:
      o.production_date &&
      o.production_date[0].end &&
      o.production_date[0].end.slice(0, 4),
    publicDomain: o.public_domain
  };
  return result;
}
function findYears(s) {
  years = [];
  !(" " + s + " ").replace(/\D[12]\d\d\d\D/, m => years.push(m.slice(1, -1)));
  years.sort();
  return { startYear: years[0], endYear: years[years.length - 1] };
}
async function nivaagaard(o) {
  const thumb = o.thumb.replace(/&#x(..);/g, (s, hex) => String.fromCharCode(parseInt(hex, 16)));
  const result = {
    id: "nivaagaard:" + o.inventory,
    name: o.title,
    creator: [o.creator.trim()],
    contributor: [],
    description: o.description,
    collection: "nivaagaard",
    materials: [o.method],
    image: [o.imageFull],
    thumbnailUrl: thumb,
    license: o.license,
    temporal: o.date,
    sameAs: o.sameAs,
    keywords: [],

    ...findYears(o.date),
    publicDomain: o.license === "PUBLIC DOMAIN"
  };
  return result;
}
async function bib(o) {
  const basisCount = o.hasPart
    .map(o => o.pid)
    .filter(o => o.startsWith("870970-basis:")).length;
  if (basisCount === 0) {
    return;
  }

  const result = {
    id: "bib:" + o.wid,
    ...o,
    collection: "bib",
    materials: _.uniq(o.hasPart.map(o => o.additionalType)),
    image: o.hasPart.map(o => o.image).filter(o => o),
    thumbnailUrl: o.image,
    license: "",
    temporal: o.datePublished[0],
    ...findYears(o.datePublished[0]),
    publicDomain: undefined
  };
  return result;
}
async function natmus(o) {
  const result = {
    id: "natmus:" + o.collection + ":" + o.objectIdentification,
    name: "",
    creator: [],
    contributor: [],
    description: "",
    collection: "natmus",
    materials: [],
    image: [],
    thumbnailUrl: "",
    license: "",
    temporal: "",
    sameAs: [],
    keywords: [],

    ...findYears(""),
    publicDomain: undefined
  };
  return;
}
async function main() {
  await processLines(__dirname + "/nivaagaard.jsonl.gz", nivaagaard);
  await processLines(__dirname + "/smk.jsonl.gz", smk);
  await processLines(__dirname + "/bib.jsonl.gz", bib);
  fs.writeFileSync(__dirname + "/works.json", JSON.stringify(ids));
}
exports.loaddata = works => {
  db = works;
  main();
};
