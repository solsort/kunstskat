const axios = require("axios");
const cheerio = require("cheerio");
const fs = require("fs");

async function bib() {
  const i = 1;
  const f = fs.createWriteStream("bib.jsonl");
  const workCount = 1166414;
  for (let i = 1; i <= workCount; ++i) {
    const data = (await axios.get(`https://bibdata.dk/collection/${i}.json`))
      .data;
    if(data.image) {
    if(data && data.hasPart && data.hasPart.length) {
      for(const k in data.hasPart) {
        data.hasPart[k] = (await axios.get(data.hasPart[k].url)).data;
      }
      f.write(JSON.stringify(data) + "\n");
    }
    }
    if(i % 1000 === 0) {
      console.log(`${i} / ${workCount} fetched.    ${(new Date()).toISOString()}`)
    }
  }
  f.end();
}
bib();

async function natmus() {
  const query = "type%3Aobject+AND+_exists_%3Arelated.assets";
  const count = (
    await axios.get(
      `http://api.natmus.dk/search/public/simple?size=1&offset=0&query=${query}`
    )
  ).data.numberOfResultsTotal;
  console.log(count);
  const f = fs.createWriteStream("natmus.jsonl");
  const i = 0;
  for (let i = 0; i < count; i += 100) {
    let result = (
      await axios.get(
        `http://api.natmus.dk/search/public/simple?size=100&offset=${i}&query=type%3Aobject+AND+_exists_%3Arelated.assets`
      )
    ).data.results.map(o => o.data);
    console.log(result);
    console.log(i, count);
    for (const o of result) {
      f.write(JSON.stringify(o) + "\n");
    }
  }
  f.end();
}

const get$ = async url => cheerio.load((await axios.get(url)).data);
async function scrapeData(img, thumb) {
  const $ = await get$("https://www.nivaagaard.dk/en/samling-en/" + img);
  //console.log($('.product-full-desc').text());
  let [date, method, inventory, received] = $(".product-full-desc > h6")
    .html()
    .replace(/[\s\S]*<\/em>[,.]?\s*/, "")
    .split("<br>")
    .map(s => s.trim());
  inventory = inventory.replace("Inventory number: ", "");
  const creator = $(".product-full-desc > h6 > strong").text();
  const title = $(".product-full-desc > h6 > em")
    .slice(0, 1)
    .text();
  const description = cheerio
    .load(
      $(".product-full-desc")
        .html()
        .replace(/<a [\s\S]*/, "")
        .replace(/[\s\S]*<\/h6>/, "")
    )
    .text()
    .trim();
  license = $(".product-full-desc a")
    .slice(1, 2)
    .text();
  image = $(".wp-post-image")
    .slice(0, 1)
    .attr("src");
  imageFull = $(".product-full-desc a")
    .slice(0, 1)
    .attr("href");

  o = {
    sameAs: ["https://www.nivaagaard.dk/en/samling-en/" + img],
    collection: "Nivaagaard",
    creator,
    title,
    date,
    method,
    inventory,
    received,
    description,
    license,
    thumb,
    image,
    imageFull
  };
  return o;
}
async function nivaagaard() {
  const f = fs.createWriteStream("nivaagaard.jsonl");
  const $ = await get$("https://www.nivaagaard.dk/en/samling/");
  const imgs = $("article > a");
  imgs.each(async (i, img) => {
    const o = cheerio(img);
    const name = cheerio(img)
      .attr("href")
      .replace("https://www.nivaagaard.dk/en/samling-en/", "");
    const thumb = o
      .html()
      .replace(/[\s\S]* data-src="/, "")
      .replace(/"[\s\S]*/, "");
    f.write(JSON.stringify(await scrapeData(name, thumb)) + "\n");
    console.log(name);
  });
}

async function smk() {
  const count = (
    await axios.get(
      `https://api.smk.dk/api/v1/art/search/?keys=*&offset=0&rows=1`
    )
  ).data.found;
  const f = fs.createWriteStream("smk.jsonl");
  for (let i = 0; i < count; i += 1000) {
    let rows = await axios.get(
      `https://api.smk.dk/api/v1/art/search/?keys=*&offset=${i}&rows=1000`
    );
    console.log(i);
    rows = rows.data.items.filter(o => o.image_thumbnail && o.image_native);
    for (const row of rows) {
      f.write(JSON.stringify(row) + "\n");
    }
  }
  f.end();
  /*
  result = result.data.items.filter(o => o.image_thumbnail);
  const keys = {};
  for(const o of result) {
    for(const key in o) {
      keys[key] = (keys[key] || 0) + 1
    }
  }
  console.log(keys, result.length);
  */
}
