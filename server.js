const express = require("express");
const level = require("level");
const cors = require("cors");
const fs = require("fs");
const { client } = require("./public_html/main.min.js");
const indexHtml = fs.readFileSync("./public_html/index.html", "utf-8");

const initDB = !fs.existsSync(__dirname + "/data/works.leveldb");
const works = level(__dirname + "/data/works.leveldb");
let workIds;
async function init() {
  if (initDB) {
    await require("./data/loaddata.js").loaddata(works);
  }
  workIds = require("./data/works.json");
}
init();

const app = express();
app.disable("x-powered-by");
app.use(cors());
app.use("/work", workHandler);
app.use("/related", relatedHandler);
app.get('/', (req, res) => {
  res.end(landing);
});
app.use(express.static(__dirname + "/public_html"));
app.listen(process.env.PORT || 8013);
console.log("started");

const randomId = () =>
  workIds[(Math.pow(Math.random(), 2.5) * workIds.length) | 0];


async function relatedHandler(req, res) {
  if (!req.url.endsWith(".json")) {
    res.status(404);
    res.end();
  }
  let id = req.url.slice(1, -5);
  id = decodeURIComponent(id);
  console.log("related", JSON.stringify(id));

  let related = [];
  for (let i = 0; i < 40; ++i) {
    related.push(randomId());
  }
  related = (await Promise.all(related.map(id => works.get(id))))
    .map(s => JSON.parse(s))
    .map(({ id, name, creator, thumbnailUrl }) => ({
      id,
      name,
      creator,
      thumbnailUrl
    }));
  res.json(related);
}
async function workHandler(req, res) {
  let work;
  let returnJSON = false;
  let id = req.url.slice(1);
  if (id.endsWith(".json")) {
    id = id.slice(0, -5);
    returnJSON = true;
  }
  id = decodeURIComponent(id);
  console.log("work", JSON.stringify(id));
  if (!id) {
    id = randomId();
  }

  try {
    work = JSON.parse(await works.get(id));
  } catch (e) {
    console.log(e, id);
  }

  if (returnJSON) {
    return res.json(work);
  }

  const data = { work };
  console.log(data);
  const { html, style } = client.render(data);
  res.end(
    indexHtml
      .replace('id="main">', 'id="main">' + html)
      .replace("exports = {}", "exports = " + JSON.stringify(data))
    //.replace('id="style">', 'id="style">' + style)
  );
}

const landing = (`
<!DOCTYPE html>
<html lang="en">
  <head>
    <link rel="canonical" href="https://kulturdata.dk">
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
  </head>
  <body>
    <style>
    body {
    font-family: sans-serif;
    text-align: center;
    }
    .container {
      display: inline-block;
      max-width: 720px;
    text-align: left;
      margin: 2ex;
    }
    </style>
    <div class=container>

<h1 id="kulturdata-.dk">Kulturdata .DK</h1>
<p>Visionen er at gøre det lettere at finde kunstværker og kulturartifakter på nettet, på tværs af kulturinstitutionerne:</p>
<ul>
<li>En unik webadresse for hvert kunstværk eller genstand, hvor værket vises og kan lede den besøgende hen til kulturinstitutionen hvor værket findes, såsom <a href="https://open.smk.dk/">SMK Open</a>, <a href="https://samlinger.natmus.dk/">Nationalmuseets Samlinger</a>, <a href="https://www.nivaagaard.dk">Nivaagaard Malerisamling</a>, eller det lokale bibliotek.</li>
<li>Inspiration – foreløbigt blot tilfældig, men planen er eksempelvis at vise værker af samme kunster, billeder der ligner visuelt, genstande fra samme periode og sted, eller bøger på biblioteket om kunstneren eller emnet, ...</li>
<li>Præsentationer af informationer om værket i ensartet form, der både kan forstås af mennesker og <a href="https://en.wikipedia.org/wiki/Semantic_Web">maskiner</a>, – dette vil også gøre at det bliver lettere at finde værket via søgemaskiner såsom google.</li>
<li>Berigelse af informationer om værkerne via machine learning, som gøre det lettere at finde det, samt at give relevant inspiration.</li>
</ul>
<p><strong>Se foreløbig udgave på <a href="https://kulturdata.dk/work/">kulturdata.dk/work/</a></strong>.</p>
<p>Projektet statede på <a href="https://hack4.dk">Hack4DK</a> 2019, – og var vinderprojektet ved denne hackathon.</p>
<p><strong>Vær med!</strong> – send en mail til kulturdata@solsort.dk, hvis du har lyst til at være med, eller blot vil have besked når der er nyt fra projektet :)</p>
<p><em>Udviklingen af projektet har ligget stille siden Hack4DK-2019, men planen er at genoptage det til Hack4DK-2020</em> <s>Forøjeblikket videreudvikles projektet af <a href="https://rasmuserik.com">Rasmus Erik</a></s>, og det vil være fedt at skabe et team med flere samarbejdspartnere, eksempelvis med domæneviden (såsom folk fra kulturinstitutionerne), flair for design, datanørder eller noget helt femte.</p>
<p>Status:</p>
<ul>
<li>Eksempel på berigelse af informationer (og lidt kode for dette) kan ses på <a href="https://gitlab.com/solsort/natmus-images/blob/master/20191123-sort-images.ipynb">gitlab.com/solsort/natmus-images</a>. Er i dialog med nationalmuseet om at gå videre med dette.</li>
<li>Den er ikke helt klart endnu, – eksempelvis er inspirationen tilfældig, der vises kun få metadata, designet er mangelfuldt, og den fører endnu ikke ordentligt videre til kulturinstitutionerne. <a href="https://search.google.com/structured-data/testing-tool#url=https%3A%2F%2Fkulturdata.dk%2Fwork%2Fsmk%253AKKS14227">Den kan dog allerede forstås af maskiner</a>.</li>
<li>Den kommer til at ligge på både: <em>kunstskat.dk</em> med kunstværker, <em>bibdata.dk</em> med biblioteksmaterialer (og erstatter dette site), samt <em>kulturdata.dk</em> for museumsgenstande og øvrige værker. Anbefalingerne kommer til at gå på tværs af disse.</li>
<li>Kildekoden kan findes på <a href="https://gitlab.com/solsort/kunstskat">gitlab.com/solsort/kunstskat</a>, hvor den videre udvikling også vil blive koordineret.</li>
</ul>

<br/><hr/><br/><br/><br/>

<h2 id="description-and-details-in-english.">Description and details in english.</h2>

    <p>
    This is a project, started at <a href="https://hack4.dk">Hack4DK 2019</a>, with the purpose of making a tool where you can explore art and cultural artifacts from different danish institutions. It also has the purpose of making the works findable in search engines, such as google. It is still in progress (this page will be replaced with the actual tool when it reaches MVP-quality).
    </p>

    <p><b>
    You can see a quick preview of its current state on:  <a href=/work/>kulturdata.dk/work/</a></b>
    </p>
    <p>
    The idea is to use the major part of the view, to actually show the work, – below that is the metadata, which you can scroll to, and in the bottom there is a list of related works.
    </p>

    DONE:
    <ul>
    <li>Experiment with machine learning for enriching the metadata <a href="https://gitlab.com/solsort/natmus-images/blob/master/20191123-sort-images.ipynb">gitlab.com/solsort/natmus-images</a>, currently just a proof-of-concept, but will most likely be developed further in collaboration with the museum.
    </li>
    <li>Extract data from SMK api</li>
    <li>Scrape Nivaagaard data</li>
    <li>Extract library data from bibdata.dk</li>
    <li>Project setup on <a href="https://gitlab.com/solsort/kunstskat">gitlab.com/solsort/kunstskat</a></li>
    <li>Normalise data models to schema.org</li>
    <li>Webserver running on kulturdata.dk and kunstskat.dk to be used for this project</li>
    <li>Simple rendering of work, (only minimal meta data so far)</li>
    <li>Set up server-side rendering of works for better indexability</li>
    <li>Setup simple view of recommendations (kind of random at the moment)</li>
    <li>Semantic markup, i.e. <a href="https://search.google.com/structured-data/testing-tool#url=https%3A%2F%2Fkulturdata.dk%2Fwork%2Fsmk%253AKKS14227">understandable by search engines</a>
    </li>

    </ul>

    TODO:
    <ul>
    <li><b>Link to the artifact on the institutions site, to lead the visitors to the museums, (and maybe also open hours, address, etc.)</b></li>
    <li>Proper design.</li>
      <li>Extra data source: National museum (started on this, but on pause due to a 10.000 item limit)</li>
      <li>Extra data source: Europeana (requested and got API key, haven't looked more into this yet) </li>
      <li>Better recommendations of works, instead of randomness</li>
      <li>Show more metadata</li>
      <li>Include markup like OGP, html-metatags etc.</li>
      <li><em>Use the kultudata.dk implementation as next version of <a href="https://bibdata.dk">bibdata.dk</a> when it has feature parity</em></li>
      <li>Make a version on kunstskat.dk, containing only art like paintings and sculptures</li>
      <li>Better showing the work <ul>
        <li>Choose between different views of same work</li>
        <li>Zoomable images for large images</li>
        <li>360° viewer for natmus images (incl. VR)</li>
        <li>3D object viewer for sculptures (incl. VR)</li>
      </ul>
      <li>Page per creator</li>
      <li>Make it searchable</li>
      <li>Move this todo-list to the gitlab project, after the presentation on SMK ;)</li>
      </li>
    </ul>
      Contact <a href="https://rasmuserik.com">Rasmus Erik</a> if you have comments, or feel like joining this hack, to make it more than a hack :)
    </div>


    </div>
  </body>
</html>
  `)
