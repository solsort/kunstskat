import React from "react";
import axios from "axios";
import ReactDOMServer from "react-dom/server";
import ReactDOM from "react-dom";

const sleep = ms => new Promise(resolve => setTimeout(resolve, ms));
const isNode = typeof window === "undefined";
const isBrowser = !isNode;

async function fetchMetaData(id) {
  try {
    return (await axios.get(`/work/${id}.json`)).data;
  } catch (e) {}
  try {
    return (
      await axios.get(
        `${
          location.host.startsWith("localhost")
            ? "http://localhost:8013"
            : "https://kulturdata.dk"
        }/work/${id}.json`
      )
    ).data;
  } catch (e) {
    console.log("error", e);
  }
}
async function fetchRelated(id) {
  try {
    return (await axios.get(`/related/${id}.json`)).data;
  } catch (e) {}
  try {
    return (
      await axios.get(
        `${
          location.host.startsWith("localhost")
            ? "http://localhost:8013"
            : "https://kulturdata.dk"
        }/related/${id}.json`
      )
    ).data;
  } catch (e) {}
}

const scrollParent = async o => {
  const p = o.target.parentNode;
  for (let i = -1; i < 1; i += 0.1) {
    p.scrollBy((window.innerWidth / 13) * (1 - Math.abs(i)), 0);
    await sleep(13);
  }
};
class WorkView extends React.Component {
  constructor() {
    super();
    this.state = {};
    this.state.works = {};
  }
  async componentDidMount() {
    let { work } = this.props;
    if (!work) {
      work = await fetchMetaData(
        decodeURIComponent(location.href.replace(/.*[/?]/, ""))
      );
    }
    this.setState({ work });

    const related = await fetchRelated(work.id);
    this.setState({ related });
  }
  render() {
    let work = this.props.work || this.state.work;
    let related = this.state.related || [];
    if (!work) {
      return <div>No data yet</div>;
    }
    const spacing = 8;
    const dim = isBrowser ? window : { innerWidth: 800, innerHeight: 600 };
    const recommendHeight = 120;

    const viewHeight = dim.innerHeight - recommendHeight - 80;
    return (
      <div itemScope="itemScope" itemType="http://schema.org/CreativeWork">
        <div id="container">
          <div
            id="view"
            style={{
              display: "flex",
              flexDirection: "row",
              height: viewHeight,
              justifyContent: "center",
              alignItems: "center"
            }}
          >
            <div style={{ display: "flex" }}>
              <img
                itemProp="image"
                className="mainImg"
                src={work.image[0]}
                style={{
                  maxHeight: viewHeight - 2 * spacing,
                  margin: spacing,
                  maxWidth: dim.innerWidth - 2 * spacing
                }}
              />
            </div>
          </div>
          <center>
            <div id="metadata">
              <h1 itemProp="name">{work.name}</h1>
              <h2 itemProp="creator">{work.creator.join(" & ")}</h2>
              <b>Kan ses på {({smk: "Statens Museum for Kunst", bib: "bibliotek.dk", nivaagaard: "Nivaagaards Malerisamling"})[work.collection] || work.collection}</b>
            </div>
          </center>
        </div>
        <div
          style={{
            height: recommendHeight + spacing
          }}
        />
        <div id="recommendations">
          <div
            style={{
              display: "inline-block",
              width: dim.innerWidth,
              height: recommendHeight,
              whiteSpace: "nowrap",
              overflowY: "scroll"
            }}
          >
            {related.map(o => {
              const thumb = encodeURI(o.thumbnailUrl)
              return (
                <a
                  key={o.id}
                  href={
                    location.href.replace(/[^?/]*$/, "") +
                    `${encodeURIComponent(o.id)}`
                  }
                >
                  <img height={recommendHeight} src={encodeURI(o.thumbnailUrl)} />
                </a>
              );
            })}
            <span
              style={{
                display: "inline-block",
                width: recommendHeight / 2,
                background: "white",
                height: recommendHeight
              }}
            />

            <span
              id="moreRelated"
              onPointerDown={scrollParent}
            >
              &nbsp;»
              <br />
              &nbsp;»
              <br />
              &nbsp;»
            </span>
          </div>
        </div>
        <style>{`
          body {
            margin: 0;
            padding: 0;
            font-family: sans-serif;
          }
          #recommendations {
            position: fixed;
            bottom: 0;
            background: black;
            height: ${recommendHeight}px;
          }
          #metadata {
            width: 100%;
            max-width: 720px;
            background: white;
          }
          #moreRelated {
            display: inline-block;
            background-image: linear-gradient(to right, 
                rgba(255,255,255,0), 
                rgba(255,255,255,0.9), 
                white);
            line-height: ${recommendHeight / 3.3}px;
            text-align: center;
            position: absolute;
            font-size: 20px;
            font-weight: 100;
            right: 0;
            top: 0;
            height: ${recommendHeight}px;
            width: ${recommendHeight / 3}px;
            scrollbar-width: none;
          }
          #moreRelated::-webkit-scrollbar {
            display: none;
          }
        `}</style>
      </div>
    );
  }
}

export function render(o) {
  return {
    html: ReactDOMServer.renderToStaticMarkup(<WorkView {...o} />)
  };
}

if (isBrowser) {
  const reactView = ReactDOM.render(
    <WorkView {...exports} />,
    document.querySelector("#main")
  );
  window.addEventListener("resize", () => reactView.forceUpdate());
}
